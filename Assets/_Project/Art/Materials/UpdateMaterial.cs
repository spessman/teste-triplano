using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

namespace _Project.Art.Materials
{
    public class UpdateMaterial : MonoBehaviour
    {
        [SerializeField] private Material _material;
    
        private float _minValueY;
        private float _maxValueY;
        private float _minValueX;
        private float _maxValueX;
    
        private float _currentTimeToChangeMaterialProperty;
        private float _currentValueForMaterialPropertyY;
        private float _currentValueForMaterialPropertyX;

        private const float _minValueChangeTime = 2f;
        private const float _maxValueChangeTime = 15f;
        private const int _lerpSlowdown = 30;
    
        private void Start()
        {
            StartCoroutine(RandomizeMaterialValue());
        
            _minValueY = _material.GetFloat("Ymin");
            _maxValueY = _material.GetFloat("Ymax");
            _minValueX = _material.GetFloat("Xmin");
            _maxValueX = _material.GetFloat("Xmax");
        }

        private void Update()
        {
            float materialAmountY = _material.GetFloat("AmountY");
            float materialAmountX = _material.GetFloat("AmountX");
        
            float newAmountY = Mathf.Lerp(materialAmountY, _currentValueForMaterialPropertyY, Time.deltaTime / _lerpSlowdown);
            float newAmountX = Mathf.Lerp(materialAmountX, _currentValueForMaterialPropertyX, Time.deltaTime / _lerpSlowdown);
        
            _material.SetFloat("AmountY" , newAmountY);
            _material.SetFloat("AmountX" , newAmountX);
        }

        private IEnumerator RandomizeMaterialValue()
        {
            for (int i = 0; i < _currentTimeToChangeMaterialProperty; i++)
            {
                yield return new WaitForSeconds(1);
            }

            RandomizeValues();
        }

        private void RandomizeValues()
        {
            StopCoroutine(RandomizeMaterialValue());
            _currentValueForMaterialPropertyY = Random.Range(_minValueY, _maxValueY);
            _currentValueForMaterialPropertyX = Random.Range(_minValueX, _maxValueX);
            _currentTimeToChangeMaterialProperty = Random.Range(_minValueChangeTime, _maxValueChangeTime);
            StartCoroutine(RandomizeMaterialValue());
        }
    }
}
