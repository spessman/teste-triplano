using System.Collections;
using System.Collections.Generic;
using _Project.Scripts.EventSystem;
using _Project.Scripts.UI_Helper;
using _Project.Scripts.Utils;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace _Project.Scripts.Core
{
    /// <summary>
    /// Manages loading scenes and the loading screen
    /// </summary>
    public sealed class GameManager : MonoBehaviour
    {
        [SerializeField] private SceneLoader sceneLoader;
        
        private void Awake()
        {
            Setup();
            SubscribeToEvents();
        }

        private void OnDestroy()
        {
            UnsubscribeFromEvents();
        }

        private void Setup()
        {
            SavefileManager.UpdateSavePath();
            SavefileManager.CreateSavefileIfNotExist();
            LoadMainMenu();
        }

        private void SubscribeToEvents()
        {
            MainMenuUIHelperEventChannel.PlayButtonPressed += LoadGameLoop;
            EndgameMenuUIHelperEventChannel.MainMenuButtonPressed += LoadMainMenu;
            EndgameMenuUIHelperEventChannel.RestartButtonPressed += LoadGameLoop;
            GameMenuUIHelperEventChannel.ExitButtonPressed += LoadMainMenu;
        }

        private void UnsubscribeFromEvents()
        {
            MainMenuUIHelperEventChannel.PlayButtonPressed -= LoadGameLoop;
            EndgameMenuUIHelperEventChannel.MainMenuButtonPressed -= LoadMainMenu;
            GameMenuUIHelperEventChannel.ExitButtonPressed -= LoadMainMenu; 
        }

        private void LoadGameLoop()
        {
            sceneLoader.LoadScene(Scenes.GAME_LOOP);
        }

        private void LoadMainMenu()
        {
            sceneLoader.LoadScene(Scenes.MAIN_MENU);
        }
    }
}
