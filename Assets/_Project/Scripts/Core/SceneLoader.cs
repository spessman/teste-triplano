using System;
using System.Collections;
using System.Collections.Generic;
using _Project.Scripts.EventSystem;
using _Project.Scripts.Utils;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace _Project.Scripts.Core
{
    public sealed class SceneLoader : MonoBehaviour
    {
        [SerializeField] private SceneLoaderEventChannel eventChannel;
        private float _sceneLoadingProgress;
        
        public void LoadScene(Scenes scene)
        {
            switch (scene)
            {
                case Scenes.MAIN_MENU:
                    TryUnloadScene(Scenes.GAME_LOOP);
                    StartCoroutine(LoadSceneCoroutine(Scenes.MAIN_MENU));
                    break;
                case Scenes.GAME_LOOP:
                    TryUnloadScene(Scenes.MAIN_MENU);
                    StartCoroutine(LoadSceneCoroutine(Scenes.GAME_LOOP));
                    break;
                case Scenes.MANAGER:
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(scene), scene, null);
            }
        }

        private static void TryUnloadScene(Scenes scene)
        {
            if (SceneManager.GetSceneByBuildIndex((int)scene).isLoaded)
            {
                SceneManager.UnloadSceneAsync((int)scene);
            }
        }
        
        private IEnumerator LoadSceneCoroutine(Scenes scene)
        {
            _sceneLoadingProgress = 0;
            eventChannel.OnSceneLoad(_sceneLoadingProgress);

            if (SceneManager.GetSceneByBuildIndex((int)scene).isLoaded)
            {
                SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex((int)Scenes.MANAGER));
                SceneManager.UnloadSceneAsync((int)scene);
            }
           
            AsyncOperation operation = (SceneManager.LoadSceneAsync((int)scene, LoadSceneMode.Additive));

            yield return new WaitUntil( () => operation.isDone);
            _sceneLoadingProgress = operation.progress;
            
            SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex((int)scene));
            eventChannel.OnSceneLoad(_sceneLoadingProgress);
        }
    }
}