using System.Collections.Generic;
using _Project.Scripts.GamePath;
using UnityEngine;

namespace _Project.Scripts.Core
{
    /// <summary>
    /// Handles object pooling of any type, with a special case where we have an game path object, and we instantiate how many path objects we define
    /// </summary>
    public sealed class ObjectPool : MonoBehaviour
    {
        public static ObjectPool singleton { get; private set; }
        
        private readonly Dictionary<string, Queue<GameObject>> _objectPool = new Dictionary<string, Queue<GameObject>>();

        private void Awake()
        {
            InitializeSingleton();
        }

        public GameObject GetObject(GameObject poolGameObject)
        {
            if (_objectPool.TryGetValue(poolGameObject.name, out Queue<GameObject> objectList))
            {
                if (objectList.Count == 0)
                {
                    return CreateNewObject(poolGameObject);
                }

                GameObject _gameObject = objectList.Dequeue();
                _gameObject.SetActive(true);
                Debug.Log("returning object: " + _gameObject.name);
                return _gameObject;
            }

            return CreateNewObject(poolGameObject);
        }

        public GameObject GetObject(GamePathObject poolGameObject, int maxInstances)
        {
            if (_objectPool.TryGetValue(poolGameObject.gameObject.name, out Queue<GameObject> objectList))
            {
                if (objectList.Count == 0)
                {
                    GameObject newObject = null;
                    for (int i = 0; i <= maxInstances; i++)
                    {
                        newObject = CreateNewObject(poolGameObject.gameObject);
                        newObject.transform.parent = transform;
                        ReturnGameObject(newObject);
                    }
                }
                
                GameObject _gameObject = objectList.Dequeue();
                _gameObject.SetActive(true);
                Debug.Log("returning object: " + _gameObject.name);
                return _gameObject;
            }
            
            return CreateNewObject(poolGameObject.gameObject);
        }

        private static GameObject CreateNewObject(GameObject poolGameObject)
        {
            GameObject newGameObject = Instantiate(poolGameObject);
            newGameObject.name = poolGameObject.name;
            return newGameObject;
        }

        public void ReturnGameObject(GameObject poolGameObject)
        {
            if (_objectPool.TryGetValue(poolGameObject.name, out Queue<GameObject> objectList))
            {
                objectList.Enqueue(poolGameObject);
            }
            else
            {
                Queue<GameObject> newObjectQueue = new Queue<GameObject>();
                newObjectQueue.Enqueue(poolGameObject);
                _objectPool.Add(poolGameObject.name, newObjectQueue);
            }

            poolGameObject.transform.parent = transform;
            poolGameObject.transform.localPosition = Vector3.zero;
            poolGameObject.SetActive(false);
        }

        private void InitializeSingleton()
        {
            if (singleton != null && singleton != this) { 
                Destroy(gameObject);
            }
            else
            {
                singleton = this;   
            }
        }
    }
}