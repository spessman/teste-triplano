using System.IO;
using UnityEngine;

namespace _Project.Scripts.Core
{
    /// <summary>
    /// Handles saving a txt file with json data for score purposes
    /// </summary>
    public static class SavefileManager
    {
        private static string savePath;
        
        public static void UpdateSavePath()
        {
            savePath = Application.dataPath + "/_Project/Savefile/save.txt";
        }

        public static void UpdateHighestStats(int cash, int distance)
        {
            SaveData highestData = LoadStats();
            SaveData newSaveData = new SaveData(cash, distance);

            if (newSaveData.TotalCash > highestData.TotalCash)
            {
                highestData.TotalCash = newSaveData.TotalCash;
            }

            if (newSaveData.TotalDistance > highestData.TotalDistance)
            {
                highestData.TotalDistance = newSaveData.TotalDistance;
            } 
            
            SaveStats(highestData);
        }
        public static void SaveStats(int cash, int distance)
        {
            SaveData newSaveData = new SaveData(cash, distance);
            
            string saveDataAsJson = JsonUtility.ToJson(newSaveData);
            File.WriteAllText(savePath, saveDataAsJson);
        }
        
        public static void SaveStats(SaveData newSaveData)
        {
            string saveDataAsJson = JsonUtility.ToJson(newSaveData);
            File.WriteAllText(savePath, saveDataAsJson);
        }
        
        public static SaveData LoadStats()
        {
            CreateSavefileIfNotExist();
            
            string saveDataString = File.ReadAllText(savePath);
            return JsonUtility.FromJson<SaveData>(saveDataString);
        }

        public static void CreateSavefileIfNotExist()
        {
            if (!File.Exists(savePath))
            {
                SaveStats(0, 0);
            }
        }
        
        public class SaveData
        {
            public int TotalCash;
            public int TotalDistance;

            public SaveData(int totalCash, int totalDistance)
            {
                TotalCash = totalCash;
                TotalDistance = totalDistance;
            }
        }
    }
}