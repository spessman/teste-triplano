using System;
using _Project.Scripts.EventSystem;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

namespace _Project.Scripts.Core
{
    public sealed class PostProcessingController : MonoBehaviour
    {
        [SerializeField] private Volume volume;

        private void Start()
        {
            SubscribeToEvents();
        }

        private void OnDestroy()
        {
            UnsubscribeFromEvents();
        }

        private void SubscribeToEvents()
        {
            SpeedBoosterEventChannel.BoostStarted += AddBoostEffects;
            SpeedBoosterEventChannel.BoostEnded += RemoveBoostEffects;
        }

        private void UnsubscribeFromEvents()
        {
            SpeedBoosterEventChannel.BoostStarted -= AddBoostEffects;
            SpeedBoosterEventChannel.BoostEnded -= RemoveBoostEffects;
        }

        private void AddBoostEffects()
        {
            volume.profile.TryGet(out LensDistortion lensDistortion);
            lensDistortion.active = true;
            lensDistortion.intensity.SetValue(new ClampedFloatParameter(-.5f, 0, 1));
            
            volume.profile.TryGet(out ChromaticAberration chromaticAberration);
            chromaticAberration.active = true;
            chromaticAberration.intensity.SetValue(new ClampedFloatParameter(1, 0, 1));
        }
        
        private void RemoveBoostEffects()
        {
            volume.profile.TryGet(out LensDistortion lensDistortion);
            lensDistortion.intensity.SetValue(new ClampedFloatParameter(0, 0, 1));
            
            volume.profile.TryGet(out ChromaticAberration chromaticAberration);
            chromaticAberration.intensity.SetValue(new ClampedFloatParameter(0, 0, 1));
        }
    }
}