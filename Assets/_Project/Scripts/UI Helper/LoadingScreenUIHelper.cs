using System;
using System.Collections.Generic;
using _Project.Scripts.Core;
using _Project.Scripts.EventSystem;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.UI_Helper
{
    public sealed class LoadingScreenUIHelper : MonoBehaviour
    {
        [SerializeField] private GameObject _loadingScreen;
        
        [SerializeField] private Slider _progressBar;

        private void Start()
        {
            SubscribeToEvents();
        }

        private void OnDestroy()
        {
            UnsubscribeFromEvents();
        }

        private void SubscribeToEvents()
        {
            SceneLoaderEventChannel.SceneLoading += UpdateLoadingScreen;
        }
        
        private void UnsubscribeFromEvents()
        {
            SceneLoaderEventChannel.SceneLoading -= UpdateLoadingScreen;
        }
        
        private void UpdateLoadingScreen(float value)
        {
            value = Mathf.RoundToInt(value * 100);
            Debug.Log(value);
                
            switch (value)
            {
                case 0:
                    _loadingScreen.SetActive(true);
                    break;
                case 100:
                    _loadingScreen.SetActive(false);
                    break;
            }

            _progressBar.value = value;
        }
    }
}