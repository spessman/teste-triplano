using _Project.Scripts.EventSystem;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.UI_Helper
{
    /// <summary>
    /// UI Helper for the ingame Menu in the top left corner
    /// </summary>
    public sealed class GameMenuUIHelper : MonoBehaviour
    {
        [SerializeField] private GameMenuUIHelperEventChannel eventChannel;
        [SerializeField] private Button exitButton;
        
        private void Start()
        {
            Setup();
        }

        private void Setup()
        {
            exitButton.onClick.AddListener(OnExitButtonPressed);
        }
        private void OnExitButtonPressed()
        {
            eventChannel.OnExitButtonPressed();
        }
    }
}