using System;
using _Project.Scripts.EventSystem;
using UnityEngine;
using Random = UnityEngine.Random;

namespace _Project.Scripts.UI_Helper
{
    public sealed class UiSoundManager : MonoBehaviour
    {
        [SerializeField] private AudioSource _audioSource;

        [SerializeField] private AudioClip _UiButtonClickSound;


        private void Start()
        {
            SubscribeToEvents();
        }

        private void OnDestroy()
        {
            UnsubscribeFromEvents();
        }

        private void SubscribeToEvents()
        {
            EndgameMenuUIHelperEventChannel.MainMenuButtonPressed += PlayUiButtonClickSound;
            EndgameMenuUIHelperEventChannel.RestartButtonPressed += PlayUiButtonClickSound;
            GameMenuUIHelperEventChannel.ExitButtonPressed += PlayUiButtonClickSound;
            MainMenuUIHelperEventChannel.PlayButtonPressed += PlayUiButtonClickSound;
            MainMenuUIHelperEventChannel.StatsButtonPressed += PlayUiButtonClickSound;
            MainMenuStatsUIHelperEventChannel.MainMenuUiStatsClosed += PlayUiButtonClickSound;
        }

        private void UnsubscribeFromEvents()
        {
            EndgameMenuUIHelperEventChannel.MainMenuButtonPressed -= PlayUiButtonClickSound;
            EndgameMenuUIHelperEventChannel.RestartButtonPressed -= PlayUiButtonClickSound;
            GameMenuUIHelperEventChannel.ExitButtonPressed -= PlayUiButtonClickSound;
            MainMenuUIHelperEventChannel.PlayButtonPressed -= PlayUiButtonClickSound;
            MainMenuUIHelperEventChannel.StatsButtonPressed -= PlayUiButtonClickSound;
            MainMenuStatsUIHelperEventChannel.MainMenuUiStatsClosed -= PlayUiButtonClickSound;
        }

        private void PlayUiButtonClickSound()
        {
            _audioSource.pitch = Random.Range(.7f, 1.4f);
            _audioSource.PlayOneShot(_UiButtonClickSound);
        }
    }
}