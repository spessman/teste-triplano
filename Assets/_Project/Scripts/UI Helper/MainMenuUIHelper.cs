using _Project.Scripts.EventSystem;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.UI_Helper
{
    /// <summary>
    /// Helps the main menu UIs
    /// </summary>
    public sealed class MainMenuUIHelper : MonoBehaviour
    {
        [SerializeField] private MainMenuUIHelperEventChannel eventChannel;
        [SerializeField] private Button playButton;
        [SerializeField] private Button statsButton;

        private void Start()
        {
            Setup();
        }

        private void Setup()
        {
            playButton.onClick.AddListener(OnPlayButtonPressed);
            statsButton.onClick.AddListener(OnStatsButtonPressed);
        }
        
        private void OnPlayButtonPressed()
        {
            eventChannel.OnPlayButtonPressed();
        }
        
        private void OnStatsButtonPressed()
        {
            eventChannel.OnStatsButtonPressed();
        }
    }
}
