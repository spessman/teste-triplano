using _Project.Scripts.Core;
using _Project.Scripts.EventSystem;
using _Project.Scripts.GamePath;
using _Project.Scripts.GamePath.PathObjects;
using _Project.Scripts.Player;
using _Project.Scripts.Roads;
using _Project.Scripts.Utils;
using TMPro;
using UnityEngine;

namespace _Project.Scripts.UI_Helper
{
    /// <summary>
    /// Helps updating the stats UI ingame
    /// </summary>
    public sealed class IngameStatsUIHelper : MonoBehaviour
    {
        [SerializeField] private TMP_Text _sessionCashText;
        [SerializeField] private int _sessionCash;

        [SerializeField] private TMP_Text _sessionDistanceText;
        [SerializeField] private int _sessionDistance;
        
        private void Start()
        {
            Setup();
            SubscribeToEvents();
        }

        private void OnDestroy()
        {
            UnsubscribeFromEvents();
        }

        private void Setup()
        {
            _sessionCash = 0;
            _sessionDistance = 0;
            _sessionCashText.text = "0";
            _sessionDistanceText.text = "0";
        }

        private void SubscribeToEvents()
        {
            CoinMultiplierEventChannel.MultiplierStarted += OnCoinMultiplierStarted;
            CoinMultiplierEventChannel.MultiplierEnded += OnCoinMultiplierEnded;
            CoinEventChannel.CoinCollected += SetSessionCashText;
            RoadEventSystem.RoadExitTriggered += SetSessionDistanceText;
            PlayerManagerEventChannel.PlayerDied += UpdateHighscore;
        }
        
        private void UnsubscribeFromEvents()
        {
            CoinMultiplierEventChannel.MultiplierStarted -= OnCoinMultiplierStarted;
            CoinMultiplierEventChannel.MultiplierEnded -= OnCoinMultiplierEnded;
            CoinEventChannel.CoinCollected -= SetSessionCashText;
            RoadEventSystem.RoadExitTriggered -= SetSessionDistanceText;
            PlayerManagerEventChannel.PlayerDied -= UpdateHighscore;
        }
        
        private void UpdateHighscore()
        {
            SavefileManager.UpdateHighestStats(_sessionCash, _sessionDistance);
        }
        
        private void SetSessionCashText(int cashValue)
        {
            _sessionCash += cashValue;
            _sessionCashText.text = _sessionCash.ToString();
        }
        
        private void SetSessionDistanceText()
        {
            _sessionDistance++;
            _sessionDistanceText.text = _sessionDistance.ToString();
        }

        private void OnCoinMultiplierStarted()
        {
            _sessionCashText.color = Color.yellow;
        }

        private void OnCoinMultiplierEnded()
        {
            _sessionCashText.color = Color.white;
        }
    }
}