using System;
using _Project.Scripts.Core;
using _Project.Scripts.EventSystem;
using _Project.Scripts.Player;
using _Project.Scripts.Roads;
using _Project.Scripts.Utils;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.UI_Helper
{
    /// <summary>
    /// Helps the UI that appears when you die
    /// </summary>
    public sealed class EndgameMenuUIHelper : MonoBehaviour
    {
        [SerializeField] private EndgameMenuUIHelperEventChannel eventChannel;
        [SerializeField] private GameObject _endGameMenuUi;

        [SerializeField] private TMP_Text _distanceRanText;
        [SerializeField] private TMP_Text _highestDistanceRanText;

        [SerializeField] private Button _mainMenuButton;
        [SerializeField] private Button _restartMenuButton;
        
        private int _distanceRan;
        
        private void Start()
        {
            Setup();
            SubscribeToEvents();
        }

        private void OnDestroy()
        {
            UnsubscribeFromEvents();
        }

        private void Setup()
        {
            _endGameMenuUi.SetActive(false);
            _endGameMenuUi.transform.localScale = Vector3.zero;
            _mainMenuButton.onClick.AddListener(OnMainMenuButtonPressed);
            _restartMenuButton.onClick.AddListener(OnRestartButtonPressed);
        }

        private void SubscribeToEvents()
        {
            RoadEventSystem.RoadExitTriggered += AddDistanceRan;
            PlayerManagerEventChannel.PlayerDied += OnEndGame;
        }

        private void UnsubscribeFromEvents()
        {
            RoadEventSystem.RoadExitTriggered -= AddDistanceRan;
            PlayerManagerEventChannel.PlayerDied -= OnEndGame;
        }

        private void AddDistanceRan()
        {
            _distanceRan++;
        }

        private void OnRestartButtonPressed()
        {
            eventChannel.OnRestartButtonPressed();
        }
        
        private void OnMainMenuButtonPressed()
        {
            eventChannel.OnMainMenuButtonPressed();
        }
        
        private void OnEndGame()
        {
            _endGameMenuUi.SetActive(true);
            UpdateStats();
                
            LeanTween.scale(_endGameMenuUi, Vector3.one, .3f);
        }

        private void UpdateStats()
        {
            _distanceRanText.text = "You ran: " + _distanceRan;
            _highestDistanceRanText.text = "Highest: " + SavefileManager.LoadStats().TotalDistance;
        }
    }
}