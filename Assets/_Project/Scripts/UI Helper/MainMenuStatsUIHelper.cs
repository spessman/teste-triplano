using System;
using _Project.Scripts.Core;
using _Project.Scripts.EventSystem;
using _Project.Scripts.Utils;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.UI_Helper
{
    /// <summary>
    /// Updates the main menu stats UI
    /// </summary>
    public sealed class MainMenuStatsUIHelper : MonoBehaviour
    {
        [SerializeField] private MainMenuStatsUIHelperEventChannel eventChannel;
        [SerializeField] private GameObject _uiGameObject;
        
        [SerializeField] private TMP_Text _totalCash;
        [SerializeField] private TMP_Text _totalDistance;

        [SerializeField] private Button _exitButton;
        
        private void Start()
        {
            Setup();
            SubscribeToEvents();
        }

        private void OnDestroy()
        {
            UnsubscribeFromEvents();
        }

        private void Setup()
        {
            _exitButton.onClick.AddListener(OnExitButtonPressed);
        }

        private void SubscribeToEvents()
        {
            MainMenuUIHelperEventChannel.StatsButtonPressed += GetStats;
        }

        private void UnsubscribeFromEvents()
        {
            MainMenuUIHelperEventChannel.StatsButtonPressed -= GetStats;
        }
        
        private void GetStats()
        {
            _uiGameObject.SetActive(true);
            
            transform.localScale = Vector3.zero;
            LeanTween.scale(gameObject, Vector3.one, .3f);
            
            SavefileManager.SaveData data = SavefileManager.LoadStats();
            _totalCash.text = data.TotalCash.ToString();
            _totalDistance.text = data.TotalDistance.ToString();
        }

        private void OnExitButtonPressed()
        {
            eventChannel.OnMainMenuUiStatsClosed();
            _uiGameObject.SetActive(false);
        }
    }
}