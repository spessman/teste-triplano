using System.Collections;
using _Project.Scripts.Core;
using _Project.Scripts.EventSystem;
using _Project.Scripts.GamePath;
using _Project.Scripts.Player;
using _Project.Scripts.Utils;
using UnityEngine;
using Random = UnityEngine.Random;

namespace _Project.Scripts.Object_Spawner
{
    /// <summary>
    /// Handles objects spawning on the path
    /// </summary>
    public sealed class PathObjectSpawner : MonoBehaviour
    {
        [SerializeField] private PathObjectSpawnerEntryCollection _entryCollection;
        [SerializeField] private Vector3 _spawnOffset;

        [SerializeField] private PathManager.Path _currentPath;

        [SerializeField] private Transform _spawnTransform;
        [SerializeField] private float _spawnInterval = 1;

        private Coroutine _updatePathObjectsCoroutine;

        private void Start()
        {
            Setup();
            SubscribeToEvents();
        }

        private void OnDestroy()
        {
            UnsubscribeFromEvents();
        }

        private void Setup()
        {
            _spawnOffset = new Vector3();
            _updatePathObjectsCoroutine = StartCoroutine(UpdatePathObjects());
        }

        private void SubscribeToEvents()
        {
            PlayerManagerEventChannel.PlayerDied += OnPlayerDeath;
        }

        private void UnsubscribeFromEvents()
        {
            PlayerManagerEventChannel.PlayerDied -= OnPlayerDeath;
        }
        private void OnPlayerDeath()
        {
            if (_updatePathObjectsCoroutine != null)
            {
                StopCoroutine(_updatePathObjectsCoroutine);
            }
        }

        private IEnumerator UpdatePathObjects()
        {
            for (int i = 0; i < 1; i--)
            {
                UpdatePathObjectInstances();
                yield return new WaitForSeconds(_spawnInterval);
            }
        }

        private void UpdatePathObjectInstances()
        {
            PathObjectSpawnerEntry randomEntry = GetRandomEntry();

            PathManager.Path _lastPath = _currentPath;
            Vector3 _newPath = GetRandomPath();
            Vector3 spawnPosition = new Vector3(_newPath.x, 0, _spawnTransform.position.z) + _spawnOffset;
            _spawnOffset = randomEntry.PathObject.Size;
            
            if (_lastPath != _currentPath)
            {
                _spawnOffset = Vector3.zero;
            }
            
            GameObject pathObject = ObjectPool.singleton.GetObject(randomEntry.PathObject, randomEntry.MaxInstances);
            pathObject.SetActive(true);
            pathObject.transform.position = spawnPosition;
            pathObject.transform.rotation = randomEntry.PathObject.Rotation;
            pathObject.transform.parent = transform;
        }

        private PathObjectSpawnerEntry GetRandomEntry()
        {
            int chance = Random.Range(0, 100);
            int randomIndex = Random.Range(0, _entryCollection.Entries.Count);
            PathObjectSpawnerEntry entry = _entryCollection.GetEntry(randomIndex);
            
            return entry.SpawnChance >= chance ? entry : GetRandomEntry();
        }
        
        private Vector3 GetRandomPath()
        {
            int randomPathIndex = Random.Range(0, 3);
            _currentPath = (PathManager.Path) randomPathIndex;
            return PathManager.singleton.GetPath(_currentPath).position;
        }
    }
}   