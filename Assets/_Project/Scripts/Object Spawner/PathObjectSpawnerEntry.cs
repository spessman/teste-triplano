using _Project.Scripts.GamePath;
using UnityEngine;

namespace _Project.Scripts.Object_Spawner
{
    [CreateAssetMenu(fileName = "PathObjectSpawnerEntry", menuName = "PathObjectSpawnerEntry", order = 0)]
    public sealed class PathObjectSpawnerEntry : ScriptableObject
    {
        public GamePathObject PathObject;
        public int MaxInstances;
        public int SpawnChance;
    }
}