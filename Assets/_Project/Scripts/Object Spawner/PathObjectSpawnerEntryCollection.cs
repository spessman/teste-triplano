using System.Collections.Generic;
using _Project.Scripts.GamePath;
using UnityEngine;

namespace _Project.Scripts.Object_Spawner
{
    [CreateAssetMenu(fileName = "PathObjectSpawnerEntryCollection", menuName = "PathObjectSpawnerEntryCollection", order = 0)]
    public sealed class PathObjectSpawnerEntryCollection : ScriptableObject
    {
        public List<PathObjectSpawnerEntry> Entries;
        public int Count => Entries.Count;

        public PathObjectSpawnerEntry GetEntry(int index) => Entries[index];
    }
}