using System;
using _Project.Scripts.EventSystem;
using _Project.Scripts.Player;
using Cinemachine;
using UnityEngine;

namespace _Project.Scripts.Utils
{
    public sealed class CameraShakeHandler : MonoBehaviour
    {
        [SerializeField] private Animator _animator;
        private int shakeAnimationId;

        private void Start()
        {
            Setup();
            SubscribeToEvents();
        }

        private void OnDestroy()
        {
            UnsubscribeFromEvents();
        }

        private void Setup()
        {
            shakeAnimationId = Animator.StringToHash("Shake");
        }
        
        private void SubscribeToEvents()
        {
            PlayerManagerEventChannel.PlayerDied += ShakeCamera;
        }

        private void UnsubscribeFromEvents()
        {
            PlayerManagerEventChannel.PlayerDied -= ShakeCamera;
        }
        
        private void ShakeCamera()
        {
            _animator.SetTrigger(shakeAnimationId);
        }
    }
}