namespace _Project.Scripts.Utils
{
    public enum Scenes
    {
        MANAGER = 0,
        MAIN_MENU = 1,
        GAME_LOOP = 2
    }
}