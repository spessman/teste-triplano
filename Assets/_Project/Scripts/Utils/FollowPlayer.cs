using UnityEngine;

namespace _Project.Scripts.Utils
{
   /// <summary>
   /// simple script to make an object follow the player
   /// </summary>
   public sealed class FollowPlayer : MonoBehaviour
   {
      [SerializeField] private Transform _player;

      private void Update()
      {
         MoveToPlayerZ();
      }

      private void MoveToPlayerZ()
      {
         Vector3 newPosition = new Vector3(transform.position.x, transform.position.y, _player.position.z);
         transform.position = newPosition;
      }
   }
}
