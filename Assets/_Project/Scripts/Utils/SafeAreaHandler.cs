using UnityEngine;

namespace _Project.Scripts.Utils
{
    /// <summary>
    /// Simply moves a rect transform's yMin down the safe area
    /// </summary>
    public sealed class SafeAreaHandler : MonoBehaviour
    {
        [SerializeField] private RectTransform rectTransform;

        private void Start()
        {
            Setup();
        }

        private void Setup()
        {
            var safeArea = Screen.safeArea;
            var anchorMin = safeArea.position;
            var anchorMax = anchorMin + safeArea.size;
            anchorMin.x /= Screen.width;
            anchorMin.y /= Screen.height;
            anchorMax.x /= Screen.width;
            anchorMax.y /= Screen.height;

            rectTransform.anchorMin = anchorMin;
            rectTransform.anchorMax = anchorMax;
        }
    }
}
