using System.Collections;
using _Project.Scripts.EventSystem;
using _Project.Scripts.GamePath;
using _Project.Scripts.Player;
using UnityEngine;

namespace _Project.Scripts.Roads
{
    /// <summary>
    /// Sends an event each time the player crosses the end of the road trigger
    /// </summary>
    public sealed class Road : MonoBehaviour
    {
        [SerializeField] private RoadEventSystem eventSystem;
        private void OnTriggerEnter(Collider other)
        {
            ProcessTriggerEnter(other);
        }

        private void ProcessTriggerEnter(Collider other)
        {
            if (other.GetComponent<PlayerManager>())
            {
                eventSystem.OnRoadExitTriggered();
            }
        }
    }
}
