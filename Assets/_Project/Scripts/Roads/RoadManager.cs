using System.Collections.Generic;
using System.Linq;
using _Project.Scripts.Core;
using _Project.Scripts.EventSystem;
using _Project.Scripts.GamePath;
using _Project.Scripts.Object_Spawner;
using _Project.Scripts.Utils;
using UnityEngine;
using Random = UnityEngine.Random;

namespace _Project.Scripts.Roads
{
    /// <summary>
    /// Handles spawning roads and keeping track of them
    /// </summary>
    public sealed class RoadManager : MonoBehaviour
    {
        [SerializeField] private Transform _initialSpawnPoint;
    
        [SerializeField] private PathObjectSpawnerEntryCollection _entryCollection;
        [SerializeField] private List<GameObject> _roadInstances;
        [SerializeField] private int _maxRoadInstances;
        [SerializeField] private Transform _firstRoad;
        [SerializeField] private Transform _lastRoad;
    
        private const int _roadSpawnOffset = 30;
        private Vector3 _roadSpawnOffsetPosition;

        private void Start()
        {
            Setup();
            SubscribeToEvents();
            SpawnRoads();
        }

        private void OnDestroy()
        {
            UnsubscribeFromEvents();
        }

        private void Setup()
        {
            if (_entryCollection == null)
                Debug.LogError("RoadManager._roadPrefab not assigned");
        
            _roadSpawnOffsetPosition = new Vector3(0, 0, _roadSpawnOffset);
            _roadInstances = new List<GameObject>();
        }

        private void SubscribeToEvents()
        {
            RoadEventSystem.RoadExitTriggered += UpdateRoadInstances;
        }

        private void UnsubscribeFromEvents()
        {
            RoadEventSystem.RoadExitTriggered -= UpdateRoadInstances;
        }
        
        private void UpdateRoadInstances()
        {
            _lastRoad = _roadInstances.ElementAt(_roadInstances.Count - 1).transform;
            SpawnRoads();
        
            _firstRoad = _roadInstances.ElementAt(0).transform;
            _roadInstances.RemoveAt(0);
            ObjectPool.singleton.ReturnGameObject(_firstRoad.gameObject);
            _firstRoad = _roadInstances.ElementAt(0).transform;
        }

        private void SpawnRoads()
        {
            if (_roadInstances.Count == 0)
            {
                _firstRoad = GetRandomRoadTransform();
                _firstRoad.position = _initialSpawnPoint.position;
                _firstRoad.rotation = Quaternion.identity;
                _firstRoad.parent = transform;
                _lastRoad = _firstRoad;
            
                _roadInstances.Add(_firstRoad.gameObject);
            }

            for (int i = _roadInstances.Count; i <= _maxRoadInstances; i++)
            {
                _lastRoad = _roadInstances.ElementAt(_roadInstances.Count - 1).transform;
                Transform newRoad = GetRandomRoadTransform();
                newRoad.position = _lastRoad.position + _roadSpawnOffsetPosition;
                newRoad.rotation = Quaternion.identity;
                newRoad.parent = transform;

                _roadInstances.Add(newRoad.gameObject);
                _lastRoad = _roadInstances.ElementAt(_roadInstances.Count - 1).transform;
            }
        }

        private PathObjectSpawnerEntry GetRandomRoad()
        {
            int chance = Random.Range(0, 100);
            int randomIndex = Random.Range(0, _entryCollection.Count);
            PathObjectSpawnerEntry randomEntry = _entryCollection.GetEntry(randomIndex);
            
            return randomEntry.SpawnChance >= chance ? randomEntry : GetRandomRoad();
        }

        private Transform GetRandomRoadTransform()
        {
            PathObjectSpawnerEntry entry = GetRandomRoad();
            return ObjectPool.singleton.GetObject(entry.PathObject, entry.MaxInstances).transform;
        }
    }
}
 