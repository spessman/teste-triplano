using UnityEngine;

namespace _Project.Scripts.Player
{
    public static class PlayerAnimations
    {
        private const string speed = "Speed";
        private const string jump = "Jump";
        private const string death = "Death";

        public static int Speed => Animator.StringToHash(speed);
        public static int Jump => Animator.StringToHash(jump);
        public static int Death => Animator.StringToHash(death);
    }
}