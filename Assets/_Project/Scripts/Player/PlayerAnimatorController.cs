using System;
using _Project.Scripts.EventSystem;
using UnityEngine;

namespace _Project.Scripts.Player
{
    public sealed class PlayerAnimatorController : MonoBehaviour
    {
        [SerializeField] private Animator _animator;
        
        private int animatorSpeedID;
        private int animatorJumpID;
        private int animatorDeathID;
        
        private void Start()
        {
            AnimationIdsSetup();
            SubscribeToEvents();
        }

        private void AnimationIdsSetup()
        {
            animatorSpeedID = PlayerAnimations.Speed;
            animatorJumpID = PlayerAnimations.Jump;
            animatorDeathID = PlayerAnimations.Death;
        }

        private void SubscribeToEvents()
        {
            PlayerMovementControllerEventChannel.JumpPerformed += OnJumpPerformed;
            PlayerMovementControllerEventChannel.MovementSpeedChanged += OnPlayerSpeedChanged;
            PlayerManagerEventChannel.PlayerDied += OnPlayerDeath;
        }
        private void UnsubscribeFromEvents()
        {
            PlayerMovementControllerEventChannel.JumpPerformed -= OnJumpPerformed;
            PlayerMovementControllerEventChannel.MovementSpeedChanged -= OnPlayerSpeedChanged;
            PlayerManagerEventChannel.PlayerDied -= OnPlayerDeath; 
        }
        private void OnDestroy()
        {
            UnsubscribeFromEvents();
        }

        private void OnPlayerSpeedChanged(float speed)
        {
            _animator.SetFloat(animatorSpeedID, speed);
        }
        private void OnPlayerDeath()
        {
            _animator.SetTrigger(animatorDeathID);
        }
        
        private void OnJumpPerformed()
        {
            _animator.SetTrigger(animatorJumpID);
        }
    }
}