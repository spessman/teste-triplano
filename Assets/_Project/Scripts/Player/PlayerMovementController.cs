using System;
using _Project.Scripts.CustomInput;
using _Project.Scripts.EventSystem;
using _Project.Scripts.GamePath;
using UnityEngine;

namespace _Project.Scripts.Player
{
    public sealed class PlayerMovementController : MonoBehaviour
    {
        [SerializeField] private PlayerMovementControllerEventChannel eventChannel;
        
        [SerializeField] private PlayerMovementData _movementData;
        [SerializeField] private PathManager.Path _selectedPath;

        [SerializeField] private float _currentSpeed;
        [SerializeField] private float _currentAcceleration;

        private void Start()
        {
            Setup();
            SubscribeToEvents();
        }

        private void OnDestroy()
        {
            UnsubscribeToEvents();
        }

        private void Setup()
        {
            _currentSpeed = PlayerMovementData.StartingSpeed;
            _currentAcceleration = PlayerMovementData.StartingAcceleration;
        }

        private void SubscribeToEvents()
        {
            
            SwipeDetectionEventChannel.SwipePerformed += ProcessDirectionInput;
        }

        private void UnsubscribeToEvents()
        {
            SwipeDetectionEventChannel.SwipePerformed -= ProcessDirectionInput;
        }
        
        public void MoveCharacterForward(float speedBoost)
        {
            float oldSpeed = _currentSpeed;
            _currentSpeed = Mathf.Clamp(_currentSpeed, _movementData.MinSpeed, _movementData.MaxSpeed);
            _currentAcceleration = Mathf.Clamp(_currentAcceleration, _movementData.MinAcceleration, _movementData.MaxAcceleration);

            _currentSpeed += _currentAcceleration * Time.deltaTime;
            _currentSpeed += speedBoost;

            if (oldSpeed != _currentSpeed)
            {
                eventChannel.OnMovementSpeedChanged(_currentSpeed);
            }

            Vector3 vectorSpeed = transform.forward * (_currentSpeed);
            
            transform.position += Vector3.Lerp(Vector3.zero, vectorSpeed, Time.deltaTime * 10);
        }

        public void MoveCharacterToPath(float speedBoost)
        {
            Vector3 selectedPathPosition = PathManager.singleton.GetPath(_selectedPath).position;
            Vector3 newPosition = selectedPathPosition;
            newPosition = new Vector3(newPosition.x, 0, transform.position.z);

            transform.position = Vector3.Lerp(transform.position, newPosition, Time.deltaTime * (10 + speedBoost));
        }
        
        private void ProcessDirectionInput(SwipeDetection.SwipeDirection direction)
        {
            switch (direction)
            {
                case SwipeDetection.SwipeDirection.up:
                    eventChannel.OnJumpPerformed();
                    break;
                case SwipeDetection.SwipeDirection.left:
                    _selectedPath--;
                    break;
                case SwipeDetection.SwipeDirection.right:
                    _selectedPath++;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(direction), direction, null);
            }

            _selectedPath = 
                (PathManager.Path) Mathf.Clamp((int) _selectedPath, 0, Enum.GetNames(typeof(PathManager.Path)).Length - 1);
        }
    }
}