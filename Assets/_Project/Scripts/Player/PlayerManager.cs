using System;
using System.Collections;
using _Project.Scripts.EventSystem;
using _Project.Scripts.GamePath;
using UnityEngine;

namespace _Project.Scripts.Player
{
    /// <summary>
    /// Manages player movement and animation
    /// </summary>
    public sealed class PlayerManager : MonoBehaviour
    {
        [SerializeField] private PlayerManagerEventChannel eventChannel;
        [SerializeField] private PlayerMovementController _movementController;
        [SerializeField] private PlayerCollectableController _collectableController;

        private bool isDead;

        private void FixedUpdate()
        {
            ProcessPlayerMovement();
        }

        private void ProcessPlayerMovement()
        {
            if (isDead)
            {
                return;
            }
            _movementController.MoveCharacterForward(_collectableController.SpeedBoost);
            _movementController.MoveCharacterToPath(_collectableController.SpeedBoost);
        }

        private void OnTriggerEnter(Collider other)
        {
            CheckForPathObject(other);
        }

        private void CheckForPathObject(Collider other)
        {
            GamePathObject pathObjectHit = other.GetComponent<GamePathObject>();
            if (pathObjectHit == null) 
                return;
            
            if (pathObjectHit.DeathOnTouch && !isDead)
            {
                eventChannel.OnPlayerDeath();
                isDead = true;
                LeanTween.moveLocalZ(gameObject, transform.position.z - 5f, .4f);
            }
            else
            {
                pathObjectHit.OnCollect(_collectableController);
            }
        }
    }
}