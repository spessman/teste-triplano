using System;
using System.Collections;
using _Project.Scripts.EventSystem;
using UnityEngine;

namespace _Project.Scripts.Player
{
    public sealed class PlayerCollectableController : MonoBehaviour
    {
        [SerializeField] private SpeedBoosterEventChannel speedBoosterEventChannel;
        [SerializeField] private CoinMultiplierEventChannel coinMultiplierEventChannel;
        
        [SerializeField] private float _currentSpeedBoost;
        [SerializeField] private float _currentSpeedBoostTimer;

        [SerializeField] private int _currentCoinMultiplier;
        [SerializeField] private int _currentCoinMultiplierTimer;

        public float SpeedBoost => _currentSpeedBoost;
        public int CoinMultiplier => _currentCoinMultiplier;

        private void Start()
        {
            Setup();
        }

        private void Setup()
        {
            _currentSpeedBoost = 0;
            _currentCoinMultiplier = 1;
        }

        public void AddTemporarySpeedBoost(float boost, int time)
        {
            _currentSpeedBoost += boost;
            _currentSpeedBoostTimer = time;
            speedBoosterEventChannel.OnBoostStarted();
            StartCoroutine(TemporarySpeedBoostTimer());
        }

        private IEnumerator TemporarySpeedBoostTimer()
        {
            for (int i = 0; i < _currentSpeedBoostTimer; i++)
            {
                yield return new WaitForSeconds(1);
            }

            speedBoosterEventChannel.OnBoostEnded();
            _currentSpeedBoost = 0;
        }

        public void AddTemporaryCoinMultiplier(int multiplier, int time)
        {
            _currentCoinMultiplier += multiplier;
            _currentCoinMultiplierTimer = time;
            coinMultiplierEventChannel.OnMultiplierStarted();
            StartCoroutine(TemporaryCoinMultiplierTimer());
        }

        private IEnumerator TemporaryCoinMultiplierTimer()
        {
            for (int i = 0; i < _currentCoinMultiplierTimer; i++)
            {
                yield return new WaitForSeconds(1);
            }

            coinMultiplierEventChannel.OnMultiplierEnded();
            _currentCoinMultiplier = 1;
        }
    }
}