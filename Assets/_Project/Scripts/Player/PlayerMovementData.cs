using UnityEngine;

namespace _Project.Scripts.Player
{
    [CreateAssetMenu(fileName = "PlayerMovementData", menuName = "Player", order = 0)]
    public sealed class PlayerMovementData : ScriptableObject
    {
        public const float StartingSpeed = 0;
        public float MinSpeed;
        public float MaxSpeed;
        
        public const float StartingAcceleration = .2f;
        public float MinAcceleration;
        public float MaxAcceleration;
    }
}