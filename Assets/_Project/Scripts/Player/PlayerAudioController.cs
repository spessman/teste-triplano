using System;
using _Project.Scripts.EventSystem;
using UnityEngine;
using Random = UnityEngine.Random;

namespace _Project.Scripts.Player
{
    public sealed class PlayerAudioController : MonoBehaviour
    {
        [SerializeField] private AudioSource _audioSource;
        [SerializeField] private AudioClip _playerDeathSound;

        private void Start()
        {
            SubscribeToEvents();
        }

        private void OnDestroy()
        {
            UnsubscribeFromEvents();
        }

        private void SubscribeToEvents()
        {
            PlayerManagerEventChannel.PlayerDied += PlayDeathSound;
        }

        private void UnsubscribeFromEvents()
        {
            PlayerManagerEventChannel.PlayerDied -= PlayDeathSound;
        }

        private void PlayDeathSound()
        {
            _audioSource.loop = false;
            _audioSource.clip = _playerDeathSound;
            _audioSource.Play();
        }
    }
}