using System;
using UnityEngine;

namespace _Project.Scripts.EventSystem
{
    [CreateAssetMenu(fileName = "SpeedBoosterEventChannel", menuName = "GameEventSystem/SpeedBoosterEventChannel", order = 0)]
    public sealed class SpeedBoosterEventChannel : ScriptableObject
    {
        public static event Action BoostStarted;
        public static event Action BoostEnded;

        public void OnBoostStarted()
        {
            BoostStarted?.Invoke();
        }

        public void OnBoostEnded()
        {
            BoostEnded?.Invoke();
        }
    }
}