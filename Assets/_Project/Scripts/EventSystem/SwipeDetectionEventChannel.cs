using _Project.Scripts.CustomInput;
using UnityEngine;

namespace _Project.Scripts.EventSystem
{
    [CreateAssetMenu(fileName = "SwipeDetectionEventChannel", menuName = "GameEventSystem/SwipeDetectionEventChannel", order = 0)]
    public sealed class SwipeDetectionEventChannel : ScriptableObject
    { 
        public static event System.Action<SwipeDetection.SwipeDirection> SwipePerformed;

        public void OnSwipePerformed(SwipeDetection.SwipeDirection direction)
        {
            SwipePerformed?.Invoke(direction);
        }
    }
}