using System;
using UnityEngine;

namespace _Project.Scripts.EventSystem
{
    [CreateAssetMenu(fileName = "PlayerMovementControllerEventChannel", menuName = "GameEventSystem/PlayerMovementControllerEventChannel", order = 0)]
    public sealed class PlayerMovementControllerEventChannel : ScriptableObject
    {
        public static event Action JumpPerformed;
        public static event Action<float> MovementSpeedChanged;

        public void OnJumpPerformed()
        {
            JumpPerformed?.Invoke();
        }

        public void OnMovementSpeedChanged(float speed)
        {
            MovementSpeedChanged?.Invoke(speed);
        }
    }
}