using System;
using UnityEngine;

namespace _Project.Scripts.EventSystem
{
    [CreateAssetMenu(fileName = "CoinMultiplierEventChannel", menuName = "GameEventSystem/CoinMultiplierEventChannel", order = 0)]
    public sealed class CoinMultiplierEventChannel : ScriptableObject
    {
        public static event Action MultiplierStarted;
        public static event Action MultiplierEnded;

        public void OnMultiplierStarted()
        {
            MultiplierStarted?.Invoke();
        }

        public void OnMultiplierEnded()
        {
            MultiplierEnded?.Invoke();
        }
    }
}