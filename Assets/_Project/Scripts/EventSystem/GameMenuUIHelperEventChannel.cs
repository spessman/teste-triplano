using UnityEngine;

namespace _Project.Scripts.EventSystem
{
    [CreateAssetMenu(fileName = "GameMenuUIHelperEventChannel", menuName = "GameEventSystem/GameMenuUIHelperEventChannel", order = 0)]
    public sealed class GameMenuUIHelperEventChannel : ScriptableObject
    {
        public static event System.Action ExitButtonPressed;

        public void OnExitButtonPressed()
        {
            ExitButtonPressed?.Invoke();
        }
    }
}