using System;
using UnityEngine;

namespace _Project.Scripts.EventSystem
{
    [CreateAssetMenu(fileName = "RoadEventSystem", menuName = "GameEventSystem/RoadEventSystem", order = 0)]
    public sealed class RoadEventSystem : ScriptableObject
    {
        public static event Action RoadExitTriggered;

        public void OnRoadExitTriggered()
        {
            RoadExitTriggered?.Invoke();
        }
    }
}