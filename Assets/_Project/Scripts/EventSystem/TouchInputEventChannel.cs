using System;
using UnityEngine;

namespace _Project.Scripts.EventSystem
{
    [CreateAssetMenu(fileName = "TouchInputEventChannel", menuName = "GameEventSystem/TouchInputEventChannel", order = 0)]
    public sealed class TouchInputEventChannel : ScriptableObject
    {
        public static event Action<Vector2, float> OnStartTouch;
        public static event Action<Vector2, float> OnEndTouch;
        
        public void StartTouch(Vector2 position, float time)
        {
            OnStartTouch?.Invoke(position,time);
        }
        
        public void EndTouch(Vector2 position, float time)
        {
            OnEndTouch?.Invoke(position,time);
        }
    }
}