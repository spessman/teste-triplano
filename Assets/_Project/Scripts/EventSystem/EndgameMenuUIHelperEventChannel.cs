using System;
using UnityEngine;

namespace _Project.Scripts.EventSystem
{
    [CreateAssetMenu(fileName = "EndgameMenuUIHelperEventChannel", menuName = "GameEventSystem/EndgameMenuUIHelperEventChannel", order = 0)]
    public sealed class EndgameMenuUIHelperEventChannel : ScriptableObject
    {
        public static event Action MainMenuButtonPressed;
        public static event Action RestartButtonPressed;

        public void OnMainMenuButtonPressed()
        {
            MainMenuButtonPressed?.Invoke();
        }

        public void OnRestartButtonPressed()
        {
            RestartButtonPressed?.Invoke();
        }
    }
}