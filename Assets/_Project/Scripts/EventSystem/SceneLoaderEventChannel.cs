using System;
using UnityEngine;

namespace _Project.Scripts.EventSystem
{
    [CreateAssetMenu(fileName = "SceneLoaderEventChannel", menuName = "GameEventSystem/SceneLoaderEventChannel", order = 0)]
    public sealed class SceneLoaderEventChannel : ScriptableObject
    {
        public static event Action<float> SceneLoading;

        public void OnSceneLoad(float value)
        {
            SceneLoading?.Invoke(value);
        }
    }
}