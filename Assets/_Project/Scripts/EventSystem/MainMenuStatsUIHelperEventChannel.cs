using System;
using UnityEngine;

namespace _Project.Scripts.EventSystem
{
    [CreateAssetMenu(fileName = "MainMenuStatsUIHelperEventChannel", menuName = "GameEventSystem/MainMenuStatsUIHelperEventChannel", order = 0)]
    public sealed class MainMenuStatsUIHelperEventChannel : ScriptableObject
    {
        public static event Action MainMenuUiStatsClosed;

        public void OnMainMenuUiStatsClosed()
        {
            MainMenuUiStatsClosed?.Invoke();
        }
    }
}