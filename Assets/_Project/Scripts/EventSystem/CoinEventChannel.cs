using System;
using UnityEngine;

namespace _Project.Scripts.EventSystem
{
    [CreateAssetMenu(fileName = "CoinEventChannel", menuName = "GameEventSystem/CoinEventChannel", order = 0)]
    public sealed class CoinEventChannel : ScriptableObject
    {
        public static event Action<int> CoinCollected;

        public void OnCoinCollected(int value)
        {
            CoinCollected?.Invoke(value);
        }
    }
}