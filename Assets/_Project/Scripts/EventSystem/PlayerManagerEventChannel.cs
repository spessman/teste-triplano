using System;
using UnityEngine;

namespace _Project.Scripts.EventSystem
{
    [CreateAssetMenu(fileName = "PlayerManagerEventChannel", menuName = "GameEventSystem/PlayerManagerEventChannel", order = 0)]
    public sealed class PlayerManagerEventChannel : ScriptableObject
    {
        public static event Action PlayerDied;

        public void OnPlayerDeath()
        {
            PlayerDied?.Invoke();
        }
    }
}