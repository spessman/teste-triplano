using UnityEngine;

namespace _Project.Scripts.EventSystem
{
    [CreateAssetMenu(fileName = "MainMenuUIHelperEventChannel", menuName = "GameEventSystem/MainMenuUIHelperEventChannel", order = 0)]
    public sealed class MainMenuUIHelperEventChannel : ScriptableObject
    {
        public static event System.Action PlayButtonPressed;
        public static event System.Action StatsButtonPressed;

        public void OnPlayButtonPressed()
        {
            PlayButtonPressed?.Invoke();
        }

        public void OnStatsButtonPressed()
        {
            StatsButtonPressed?.Invoke();
        }
    }
}