using _Project.Scripts.EventSystem;
using UnityEngine;

namespace _Project.Scripts.CustomInput
{
    /// <summary>
    /// Handles the Swipe action, using the events from TouchInputManager
    /// </summary>
    public sealed class SwipeDetection : MonoBehaviour
    {
        [SerializeField] private SwipeDetectionEventChannel eventChannel;
        [SerializeField] private float _minimumSwipeDistance;
        [SerializeField] private float _maximumSwipeTime;
        [SerializeField] private float _directionThreshold;
        
        private TouchInputManager _inputManager;
        
        private Vector2 _startPosition;
        private float _startTime;
        private Vector2 _endPosition;
        private float _endTime;

        public enum SwipeDirection
        {
            left = 0,
            right = 1,
            up = 2
        }
        
        private void Awake()
        {
            Setup();
        }
        
        private void OnEnable()
        {
            SubscribeToEvents();
        }
        
        private void OnDisable()
        {
            UnsubscribeFromEvents();
        }

        private void Setup()
        {
            _inputManager = TouchInputManager.singleton;
        }

        private void SubscribeToEvents()
        {
            TouchInputEventChannel.OnStartTouch += SwipeStart;
            TouchInputEventChannel.OnEndTouch += SwipeEnd;
        }

        private void UnsubscribeFromEvents()
        {
            TouchInputEventChannel.OnStartTouch -= SwipeStart;
            TouchInputEventChannel.OnEndTouch -= SwipeEnd;
        }
        
        private void SwipeStart(Vector2 position, float time)
        {
            Debug.Log("Swipe Start");
            _startPosition = position;
            _startTime = time;
        }

        private void SwipeEnd(Vector2 position, float time)
        {
            Debug.Log("Swipe End");
            _endPosition = position;
            _endTime = time;
            DetectSwipe();
        }

        private void DetectSwipe()
        {
            if (Vector3.Distance(_startPosition, _endPosition) >= _minimumSwipeDistance 
                && (_endTime - _startTime) <= _maximumSwipeTime)
            {
                Vector3 direction = _endPosition - _startPosition;
                Vector2 direction2D = new Vector2(direction.x, direction.y).normalized;
                ProcessSwipeDirection(direction2D);
            }
        }

        private void ProcessSwipeDirection(Vector2 direction)
        {
            if (Vector2.Dot(Vector2.left, direction) > _directionThreshold)
            {
                eventChannel.OnSwipePerformed(SwipeDirection.left);
            }
            else if (Vector2.Dot(Vector2.right, direction) > _directionThreshold)
            {
                eventChannel.OnSwipePerformed(SwipeDirection.right);
            }
            else if (Vector2.Dot(Vector2.up, direction) > _directionThreshold)
            {
                eventChannel.OnSwipePerformed(SwipeDirection.up);
            }
        }
    }
}