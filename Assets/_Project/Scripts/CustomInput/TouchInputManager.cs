using _Project.Scripts.EventSystem;
using UnityEngine;
using UnityEngine.InputSystem;

namespace _Project.Scripts.CustomInput
{
    /// <summary>
    /// Handles the touch input from the Touch Controls
    /// </summary>
    [DefaultExecutionOrder(-10)]
    public sealed class TouchInputManager : MonoBehaviour
    {
        public static TouchInputManager singleton { get; private set; }

        [SerializeField] private TouchInputEventChannel eventChannel;
        private TouchControls _touchControls;

        private Camera _camera;
    
        private void Awake()
        {
            InitializeSingleton();
            Setup();   
        }

        private void OnEnable()
        {
            SubscribeToEvents();
        }

        private void OnDisable()
        {
            UnsubscribeFromEvents();
        }

        private void Setup()
        {
            _touchControls = new TouchControls();
            _camera = Camera.main;
        }

        private void SubscribeToEvents()
        {
            _touchControls.Enable();
            _touchControls.Touch.PrimaryContact.started += StartTouchPrimary;
            _touchControls.Touch.PrimaryContact.canceled += EndTouchPrimary;
        }

        private void UnsubscribeFromEvents()
        {
            _touchControls.Disable();
            _touchControls.Touch.PrimaryContact.started -= StartTouchPrimary;
            _touchControls.Touch.PrimaryContact.canceled -= EndTouchPrimary;
        }
        
        private void StartTouchPrimary(InputAction.CallbackContext context)
        {
            eventChannel.StartTouch(ScreenToWorld(_camera, _touchControls.Touch.PrimaryPosition.ReadValue<Vector2>()), (float) context.startTime);
        }
    
        private void EndTouchPrimary(InputAction.CallbackContext context)
        { 
            eventChannel.EndTouch(ScreenToWorld(_camera, _touchControls.Touch.PrimaryPosition.ReadValue<Vector2>()), (float) context.time);
        }

        private Vector3 ScreenToWorld(Camera mainCamera, Vector3 position)
        {
            position.z = mainCamera.nearClipPlane;
            return mainCamera.ScreenToWorldPoint(position);
        
        }
    
        private void InitializeSingleton()
        {
            if (singleton != null && singleton != this) { 
                Destroy(gameObject);
            }
            else
            {
                singleton = this;   
            }
        }
    
    }
}
