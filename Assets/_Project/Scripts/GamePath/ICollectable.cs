using _Project.Scripts.Player;

namespace _Project.Scripts.GamePath
{
    public interface ICollectable
    {
        public void OnCollect(PlayerCollectableController collectableController);
    }
}