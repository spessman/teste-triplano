using System.Collections;
using UnityEngine;

namespace _Project.Scripts.GamePath.Helper
{
    /// <summary>
    /// Handles destroying a path object once it hits the triggers
    /// </summary>
    public sealed class PathObjectDestroyHandler : MonoBehaviour
    {
        private void OnTriggerEnter(Collider other)
        {
            ReturnObjectToObjectPool(other);
        }

        private void ReturnObjectToObjectPool(Collider other)
        {
            GamePathObject pathObject = other.GetComponent<GamePathObject>();
         
            if (pathObject != null)
            {
                pathObject.ReturnObjectToPool();
            }
        }
    }
}
