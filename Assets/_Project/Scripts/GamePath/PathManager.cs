using UnityEngine;

namespace _Project.Scripts.GamePath
{
    /// <summary>
    /// Provides path data for other classes, path position and path enum
    /// </summary>
    public sealed class PathManager : MonoBehaviour
    {
        public static PathManager singleton { get; private set; }

        [SerializeField] private Transform _pathLeft;
        [SerializeField] private Transform _pathMiddle;
        [SerializeField] private Transform _pathRight;
        
        public enum Path
        {
            left = 0,
            middle = 1,
            right = 2
        }
        private void Awake()
        {
            InitializeSingleton();
        }

        public Transform GetPath(Path path)
        {
            return path switch
            {
                Path.left => _pathLeft,
                Path.middle => _pathMiddle,
                Path.right => _pathRight,
                _ => null
            };
        }
    
        private void InitializeSingleton()
        {
            if (singleton != null && singleton != this) { 
                Destroy(gameObject);
            }
            else
            {
                singleton = this;   
            }
        }
    }
}
