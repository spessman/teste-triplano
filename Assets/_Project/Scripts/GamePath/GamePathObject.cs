using _Project.Scripts.Core;
using _Project.Scripts.Player;
using _Project.Scripts.Utils;
using UnityEngine;

namespace _Project.Scripts.GamePath
{
    /// <summary>
    /// Objects that are spawned along the path and able to interact with the player
    /// </summary>
    public class GamePathObject : MonoBehaviour, ICollectable
    {
        [SerializeField] private PathObjectData data;
        
        [SerializeField] private protected Animator _animator;
        [SerializeField] private protected AudioSource _audioSource;
        
        private const float scaleOutTime = .1f;
        
        private const float randomPitchMin = .8f;
        private const float randomPitchMax = 1.2f;
        
        public Vector3 Size => data.Size;
        public Quaternion Rotation => data.Rotation;
        public bool DeathOnTouch => data.DeathOnTouch;
        
        private void OnDisable()
        {
            SetAnimatorEnabled(false);
        }

        private void OnEnable()
        {
            SetAnimatorEnabled(true);
        }

        private protected void SetAnimatorEnabled(bool state)
        {
            _animator.enabled = state;
        }

        public virtual void OnCollect(PlayerCollectableController collectableController)
        {
            PlayCollectAudio();
            SetAnimatorEnabled(false);
            MoveUp();
        }

        private void MoveUp()
        {
            LeanTween.moveLocalY(gameObject, 10, .5f);;
        }
        
        public void ReturnObjectToPool()
        {
            ObjectPool.singleton.ReturnGameObject(gameObject);
        }

        private protected void PlayCollectAudio()
        {
            if (_audioSource == null)
            {
                return;
            }

            _audioSource.pitch = Random.Range(randomPitchMin, randomPitchMax);
            _audioSource.PlayOneShot(_audioSource.clip);
        }
    }
}