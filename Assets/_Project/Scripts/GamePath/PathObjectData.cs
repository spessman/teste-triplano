using UnityEngine;

namespace _Project.Scripts.GamePath
{
    [CreateAssetMenu(fileName = "PathObjectData", menuName = "PathObjectData", order = 0)]
    public sealed class PathObjectData : ScriptableObject
    {
        [SerializeField] private Vector3 _size;
        [SerializeField] private Vector3 _rotationOffset;
        [SerializeField] private bool _deathOnTouch = false;
        
        public Vector3 Size => _size;
        public Quaternion Rotation => Quaternion.Euler(_rotationOffset);
        public bool DeathOnTouch => _deathOnTouch;
    }
}