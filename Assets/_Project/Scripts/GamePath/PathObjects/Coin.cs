using System;
using _Project.Scripts.EventSystem;
using _Project.Scripts.Player;
using UnityEngine;

namespace _Project.Scripts.GamePath.PathObjects
{
    /// <summary>
    /// A plunger path object, is used as a coin for the player to collect
    /// </summary>
    public sealed class Coin : GamePathObject
    {
        [SerializeField] private CoinEventChannel eventChannel;

        [SerializeField] private MeshFilter _meshFilter;
        [SerializeField] private Mesh _normalMesh;
        [SerializeField] private Mesh _multiplierMesh;
        
        [SerializeField] private int _CoinValue = 1;

        private void Start()
        {
            SubscribeToEvents();
        }

        private void OnDestroy()
        {
            UnsubscribeFromEvents();
        }

        private void SubscribeToEvents()
        {
            CoinMultiplierEventChannel.MultiplierStarted += ChangeMeshToMultiplier;
            CoinMultiplierEventChannel.MultiplierEnded += ChangeMeshToNormal;
        }

        private void UnsubscribeFromEvents()
        {
            CoinMultiplierEventChannel.MultiplierStarted -= ChangeMeshToMultiplier;
            CoinMultiplierEventChannel.MultiplierEnded -= ChangeMeshToNormal;
        }
        
        public override void OnCollect(PlayerCollectableController collectableController)
        {
            base.OnCollect(collectableController);
            eventChannel.OnCoinCollected(_CoinValue * collectableController.CoinMultiplier);
        }

        private void ChangeMeshToNormal()
        {
            _meshFilter.mesh = _normalMesh;
        }
        
        private void ChangeMeshToMultiplier()
        {
            _meshFilter.mesh = _multiplierMesh;
        }
    }
}