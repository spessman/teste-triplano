using _Project.Scripts.EventSystem;
using _Project.Scripts.Player;
using UnityEngine;

namespace _Project.Scripts.GamePath.PathObjects
{
    public sealed class SpeedBooster : GamePathObject
    {
            
        [SerializeField] private float _speedBoost;
        [SerializeField] private int _speedBoostDuration;
        
        public override void OnCollect(PlayerCollectableController collectableController)
        {
            base.OnCollect(collectableController);
            collectableController.AddTemporarySpeedBoost(_speedBoost, _speedBoostDuration);
        }
    }
}