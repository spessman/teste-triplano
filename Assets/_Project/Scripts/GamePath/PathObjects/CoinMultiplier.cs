using _Project.Scripts.EventSystem;
using _Project.Scripts.Player;
using UnityEngine;

namespace _Project.Scripts.GamePath.PathObjects
{
    public sealed class CoinMultiplier : GamePathObject
    {
        
        [SerializeField] private int _multiplier;
        [SerializeField] private int _coinMultiplierDuration;
        
        public override void OnCollect(PlayerCollectableController collectableController)
        {
            base.OnCollect(collectableController);
            collectableController.AddTemporaryCoinMultiplier(_multiplier, _coinMultiplierDuration);
        }
    }
}